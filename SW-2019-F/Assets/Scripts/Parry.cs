﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GamepadInput;

public class Parry : MonoBehaviour
{
    public bool entered, parryable, exited,Wasparried,startDelay;
    GameObject ball;
    public GameObject player;
    public float multiplier = 1.5f;
    Player1Controller player1Controller;
    public float delayTimer = 0.5f;
    public bool BallHIt;
    float delay;
    void Start()

    {
        entered = false;
        Wasparried = false;
        startDelay = false;
        delay = delayTimer;
        player1Controller = player.GetComponent<Player1Controller>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (parryable && entered)
        {
            if (GamePad.GetButtonDown(GamePad.Button.A, GamePad.Index.Any))
            {
                Wasparried = true;
                Debug.Log("here******************************************************");
            }
            
        }

        if (player1Controller.ballHit)
        {
            startDelay = true;
        }
    }

    private void FixedUpdate()
    {
        BallHIt = player1Controller.ballHit;
        if (startDelay)
        {
            delay -= Time.deltaTime;
        }
        if (player1Controller.ballHit && delay <= 0 && Wasparried)
        {
            ball.GetComponent<Rigidbody2D>().velocity *= multiplier;
            player1Controller.ballHit = false;
            delay = delayTimer;
            Wasparried = false;
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Ball")
        {
            entered = !entered;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Ball")
        {
            exited = true;
            entered = false;
            parryable = false;
            player1Controller.ballHit = false;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Ball")
        {
            parryable = true;
            ball = collision.gameObject;
        }
    }
}
