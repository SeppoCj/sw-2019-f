﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField]
    float speed = 1;
    Rigidbody2D rb;
    [SerializeField]
    float rotateSpeed = 1.5f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    public void MovePlayer(float h, float v)
    {
        rb.velocity = new Vector2(h, v) * speed;
    }

    public void ParryAnimation(string triggerPressed)
    {

        if (triggerPressed == "Right")
        {
            transform.rotation = transform.rotation * Quaternion.Euler(0, 0, rotateSpeed);
        } else if (triggerPressed == "Left")
        {
            transform.rotation = transform.rotation * Quaternion.Euler(0, 0, -rotateSpeed);
        }

    }


}
