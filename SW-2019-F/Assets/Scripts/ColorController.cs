﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorController : MonoBehaviour
{
    Color startColor;
    public Color redTint;
    public Color blueTint;
    SpriteRenderer sr;
    //public bool isRed;
    public bool isBlue;


    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {

        if (isBlue)
        {
            sr.color = blueTint;
        }
        else
        {
            sr.color = redTint;
        }
        startColor = sr.color;
    }

    // Update is called once per frame
    void Update()
    {

        if (isBlue)
        {
            sr.color = blueTint;
        }
        else
        {
            sr.color = redTint;
        }
    }

    public void SwitchColor()
    {
        if (isBlue)
        {
            isBlue = false;
            sr.color = redTint;
        }
        else
        {
            isBlue = true;
            sr.color = blueTint;
        }

    }
}
