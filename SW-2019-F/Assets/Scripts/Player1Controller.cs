﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player1Controller : MonoBehaviour
{
    public bool ballHit;
    Movement moveScript;
    ColorController colorScript;

    float horizontal, vertical;
    public bool rotating = false;
    string lastTriggerPressed;
    public GameManager gmScript;

    // Start is called before the first frame update
    void Start()
    {
        moveScript = GetComponent<Movement>();
        colorScript = GetComponent<ColorController>();
    }

    // Update is called once per frame
    void Update()
    {
        horizontal = Input.GetAxisRaw("L_XAxis_0");
        vertical = Input.GetAxisRaw("L_YAxis_0");

        //REALLY UGLY IF STATEMENT BLOCK I'M QUARANTINING FOR HOPEFUL CLEANUP/////////////////////////////////////////////////
        if (Input.GetAxisRaw("TriggersR_0") >= 0.8)
        {
            rotating = true;
            lastTriggerPressed = "Right";
        }

        if (Input.GetAxisRaw("TriggersL_0") >= 0.8)
        {
            rotating = true;
            lastTriggerPressed = "Left";
        }

       // if (Input.GetAxisRaw("TriggersR_0") >= 0.8 && Input.GetAxisRaw("TriggersL_0") >= 0.8)
       // {
       //     rotating = false;
       // }
        if (Input.GetAxisRaw("TriggersR_0") <= 0.4 && Input.GetAxisRaw("TriggersL_0") <= 0.4)
        {
           rotating = false;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }

    private void FixedUpdate()
    {
        moveScript.MovePlayer(horizontal, vertical);
        if (rotating)
        {
            moveScript.ParryAnimation(lastTriggerPressed);
        }
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        ballHit = true;

        if (collision.gameObject.name == "Ball")
        {
            gmScript.CountCollision();
        }

    }


}
