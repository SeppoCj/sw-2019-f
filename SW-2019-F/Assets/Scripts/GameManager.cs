﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    int randomSelect;
    int collisionCount;
    public GameObject[] colorObjects;


    private void Awake()
    {
        collisionCount = 0;
        randomSelect = Random.Range(0, 2);
        colorObjects = GameObject.FindGameObjectsWithTag("Colorable");

        if (randomSelect > 0)
        {
            for (int i = 0; i < colorObjects.Length; i++)
            {
                colorObjects[i].GetComponent<ColorController>().isBlue = true;
            }
        }

        if (randomSelect == 0)
        {
            for (int i = 0; i < colorObjects.Length; i++)
            {
                colorObjects[i].GetComponent<ColorController>().isBlue = false;
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void CountCollision()
    {
        if (collisionCount < 2)
        {
            collisionCount += 1;
        }

        if (collisionCount >= 2)
        {
            ChangeColors();
            collisionCount = 0;
        }
    }

    void ChangeColors()
    {
        for (int i = 0; i < colorObjects.Length; i++)
        {

            colorObjects[i].GetComponent<ColorController>().SwitchColor();
        }
    }
}
